const { Product } = require('../models')

module.exports = class productController {

    static async fetchOne(req, res){
        const {id} = req.params
        try {
            const result = await Product.findByPk(+id)
            res.status(200).json({data:result})
        } catch (err) {
            res.status(500).json({message:err.message})
        }
    }

    static async createOne(req, res){
        const {name, category, rating, image, price, sold, shopId} = req.body
        try {
            const result = await Product.create({name, category, rating, image, price, sold, shopId})
            res.status(200).json({data:result})
        } catch (err) {
            res.status(500).json({message:err.message})
        }
    }

    static async editOne(req, res){
        const {name, category, rating, image, price, sold, shopId} = req.body
        const {id} = req.params
        try {
            const result = await Prodduct.update({name, category, rating, image, price, sold, shopId}, {where:{id}})
            res.status(200).json({message:`${result} product has been updated`})
        } catch (err) {
            res.status(500).json({message:err.message})
        }
    }

    static async delete(req, res){
        const {id} = req.params
        try {
            const result = await Product.destroy({where:{id}})
            res.status(200).json({message:`${result} product has been deleted`})
        } catch (err) {
            res.status(500).json({message:err.message})
        }
    }
}