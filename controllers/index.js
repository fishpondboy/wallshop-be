const shop = require('./shop_controller')
const product = require('./product_controller')

module.exports = { shop, product }