const { Shop, Product } = require("../models");
const axios = require("axios");

module.exports = class shopController {
	static async fetchAll(req, res) {
		const { userLng, userLat } = req.body;

		try {
			const page = parseInt(req.query.page) || 1;
			const limit = 10;
			const documentsCount = await Shop.count();
			const totalPages = Math.ceil(documentsCount / limit);

			if (page <= 0) throw new Error("Invalid page number");
			const offset = (page - 1) * limit;

			const Shops = await Shop.findAll({ limit, offset });
			if (!Shops) throw new Error("No Shops");
			const shopsData = Shops.map((shop) => {
				return shop.dataValues;
			});

			for (let i = 0; i < shopsData.length; i++) {
				const coordinates = `${userLng},${userLat};${shopsData[i].lng},${shopsData[i].lat}`;
				const apiUrl = `http://router.project-osrm.org/trip/v1/driving/${coordinates}?source=first&destination=last`;
				let osm = await axios.get(apiUrl);
				let distance = osm.data.trips[0].distance;
				shopsData[i]["distance"] = distance;
				delete shopsData[i].updatedAt;
				delete shopsData[i].createdAt;
				delete shopsData[i].lat;
				delete shopsData[i].lng;
			}
			res.status(200).json({ data: shopsData, totalDocuments: documentsCount, totalPages });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async fetchAllWithoutDistance(req, res) {
		try {
			const page = parseInt(req.query.page) || 1;
			const limit = 10;
			const documentsCount = await Shop.count();
			const totalPages = Math.ceil(documentsCount / limit);

			if (page <= 0) throw new Error("Invalid page number");
			const offset = (page - 1) * limit;

			const Shops = await Shop.findAll({ limit, offset });
			if (!Shops) throw new Error("No Shops");

			res.status(200).json({ data: Shops, totalDocuments: documentsCount, totalPages });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async fetchOne(req, res) {
		const { id } = req.params;
		try {
			const shop = await Shop.findByPk(+id);
			res.status(200).json({ data: shop });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async createOne(req, res) {
		const { name, category, details, rating, image, lat, lng } = req.body;
		try {
			const result = await Shop.create({ name, category, details, rating, image, lat, lng });
			res.status(200).json({ data: result });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async editOne(req, res) {
		const { name, category, details, rating, image, lat, lng } = req.body;
		const { id } = req.params;
		try {
			const result = await Shop.update(
				{ name, category, details, rating, image, lat, lng },
				{ where: { id } }
			);
			res.status(200).json({ message: `${result} shop has been updated` });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async delete(req, res) {
		const { id } = req.params;
		try {
			const result = await Shop.destroy({ where: { id } });
			res.status(200).json({ message: `${result} shop has been deleted` });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}

	static async fetchProducts(req, res) {
		try {
			const shopId = req.params.id;
			const page = parseInt(req.query.page) || 1;
			const limit = 10;
			const documentsCount = await Product.count({ where: { shopId } });
			const totalPages = Math.ceil(documentsCount / limit);
			if (page <= 0) throw new Error("Invalid page number");
			const offset = (page - 1) * limit;

			const products = await Product.findAll({ where: { shopId }, limit, offset });
			if (!products) throw new Error("No Products");
			const productsData = products.map((product) => {
				return product.dataValues;
			});
			for (let i = 0; i < productsData.length; i++) {
				delete productsData[i].updatedAt;
				delete productsData[i].createdAt;
			}

			res.status(200).json({ data: products, totalDocuments: documentsCount, totalPages });
		} catch (err) {
			res.status(500).json({ message: err.message });
		}
	}
};
