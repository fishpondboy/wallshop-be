'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Shops', [{
      name: 'Ace Hardware Mantos',
      category: 'Furniture',
      details: 'Toko Perlengkapan Rumah',
      rating: 4.8,
      image: 'https://upload.wikimedia.org/wikipedia/commons/a/ad/Ace_Hardware_Logo.svg',
      lat: '1.4728753',
      lng: '124.8295035',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Sport Station',
      category: 'Olahraga',
      details: 'Toko Perlengkapan Olahraga',
      rating: 4.8,
      image: 'https://www.map.co.id/wp-content/uploads/2015/05/AC_SportStation1.png',
      lat: '1.4721723',
      lng: '124.8291493',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('Shops', null, {})
  }
};
