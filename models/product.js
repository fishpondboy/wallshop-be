'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.belongsTo(models.Shop, { foreignKey: 'shopId' })
    }
  };
  Product.init({
    name: DataTypes.STRING,
    category: DataTypes.STRING,
    rating: DataTypes.DOUBLE,
    image: DataTypes.STRING,
    price: DataTypes.INTEGER,
    sold: DataTypes.INTEGER,
    shopId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};