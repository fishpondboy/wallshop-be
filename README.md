# Wallshop Server API

&nbsp;

## RESTful endpoints

### GET /shop

> Get all shops without distance

_Request Query_

```
{
  "page": "<Which Page to show>"
}
```

_Response (200 - Success)_

```
{
  "data":[List of Shops],
  "totalDocuments":<Total Shops>,
  "totalPages":<TotalPages>
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### POST /shop

> Get all shops

_Request Body_

```
{
  "lat": "<Latitude of User>",
  "lng": "<Longitude of User>"
}
```

_Request Query_

```
{
  "page": "<Which Page to show>"
}
```

_Response (200 - Success)_

```
{
  "data":[List of Shops],
  "totalDocuments":<Total Shops>,
  "totalPages":<TotalPages>
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### POST /shop/add

> Add new Shop

_Request Body_

```
{
    "name": "Shop Name",
    "category":"Shop Category",
    "details:"Shop Details",
    "rating":<Shop Ratings>,
    "image":"Shop Image Path/URL",
    "lat":"Shop Location's Latitude",
    "lng":"Shop Location's Longitude
}
```

_Response (200)_

```
{
    "id": <Auto Generated ID>,
    "name": "Shop Name",
    "category":"Shop Category",
    "details:"Shop Details",
    "rating":<Shop Ratings>,
    "image":"Shop Image Path/URL",
    "lat":"Shop Location's Latitude",
    "lng":"Shop Location's Longitude
    "createdAt": <Auto Generated Timestamp>,
    "updatedAt": <Auto Generated Timestamp>,
}
```

_Response (500)_

```
{
  "message": <Error Mesage>
}
```

---

### PUT /shop/:id

> Edit Shop Data by ID

_Request Body_

```
{
    "name": "Shop Name",
    "category":"Shop Category",
    "details:"Shop Details",
    "rating":<Shop Ratings>,
    "image":"Shop Image Path/URL",
    "lat":"Shop Location's Latitude",
    "lng":"Shop Location's Longitude
}
```

_Response (200)_

```
{
    "message":"<Number> shop has been updated"
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### DELETE /shop/:id

> Delete One SHop

_Response (200)_

```
{
    message:"<Number> shop has been deleted"
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### GET /shop/:id

> Get a Shop

_Response (200)_

```
{
    "data":<Shop Data>

}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### GET /shop/:id/products

> Get All Products From a Shop

_Request Query_

```
{
  "page": "<Which Page to show>"
}
```

_Response (200)_

```
{
    "data":[List of Products],
    "totalDocuments":<Total Shops>,
    "totalPages":<TotalPages>

}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### POST /product

> Add new Product

_Request Body_

```
{
    "name":"Product Name",
    "category":"Product Category",
    "rating":"Product Rating",
    "image":"Product Image path/URL",
    "price":<Product Price>,
    "sold":<Quantity of Products that has been sold>,
    "shopId":"ID of the product's Shop"
}
```

_Response (200)_

```
{
    "id": <Auto Generated ID>,
    "name":"Product Name",
    "category":"Product Category",
    "rating":"Product Rating",
    "image":"Product Image path/URL",
    "price":<Product Price>,
    "sold":<Quantity of Products that has been sold>,
    "shopId":"ID of the product's Shop",
    "createdAt": <Auto Generated Timestamp>,
    "updatedAt": <Auto Generated Timestamp>,
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### PUT /product/:id

> Edit a Product

_Request Body_

```
{
    "name":"Product Name",
    "category":"Product Category",
    "rating":"Product Rating",
    "image":"Product Image path/URL",
    "price":<Product Price>,
    "sold":<Quantity of Products that has been sold>,
    "shopId":"ID of the product's Shop"
}
```

_Response (200)_

```
{
  "message": "<Number> product has been updated"
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### DELETE /product/:id

> Delete a Product

_Response (200)_

```
{
  "message":"<Number> product has been deleted"
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```

---

### GET /product/:id

> Get a Product

_Response (200)_

```
{
  "data":<Product Data>
}
```

_Response (500)_

```
{
  "message": <Error Message>
}
```
