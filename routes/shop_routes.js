const express = require("express");
const router = express.Router();
const { shop } = require("../controllers");

router.post("/", shop.fetchAll);
router.post("/add", shop.createOne);
router.put("/:id", shop.editOne);
router.delete("/:id", shop.delete);
router.get("/:id", shop.fetchOne);
router.get("/:id/products", shop.fetchProducts);
router.get("/", shop.fetchAllWithoutDistance);

module.exports = router;
