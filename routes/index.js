const express = require('express')
const router = express.Router()

const shopRoutes = require('./shop_routes')
const productRoutes = require('./product_routes')

router.use('/shop', shopRoutes);
router.use('/product', productRoutes);
router.get('/',(_,res)=>{
    res.status(200).json({message:"Wallshop Data API"})
})

module.exports = router
