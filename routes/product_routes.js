const express = require('express');
const router = express.Router();
const { product } = require('../controllers')

router.post('/',product.createOne)
router.put('/:id',product.editOne)
router.delete('/:id',product.delete)
router.get('/:id', product.fetchOne)

module.exports = router
